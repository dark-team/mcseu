import com.darkteam.mcseu.McSeuNavigator;
import com.darkteam.mcseu.model.ServerDescriptionPage;
import com.darkteam.mcseu.model.ServerEntry;
import com.darkteam.mcseu.Sort;
import org.junit.Test;

import java.io.IOException;

public class ATest {

    @Test
    public void test(){
        try {
            ServerEntry[] serverEntries = new McSeuNavigator().getServers(1, Sort.CREATED_DESCENDING);
            for(ServerEntry serverEntry : serverEntries){
                System.out.println("---");
                System.out.println("Server Name:" + serverEntry.getServerName());
                System.out.println("MCSEU Link:" + serverEntry.getLink());
                System.out.println("Online: " + serverEntry.isOnline());
                ServerDescriptionPage description = serverEntry.getDescription();
                System.out.println("Version: " + description.getVersion());
                System.out.println("Host: " + description.getHost());
                System.out.println("Port: " + description.getPort());
                System.out.println("Long Description: " + description.getLongDescription());
                System.out.println("---\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
