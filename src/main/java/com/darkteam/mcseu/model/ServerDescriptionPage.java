package com.darkteam.mcseu.model;

import com.darkteam.mcseu.model.ServerEntry;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@AllArgsConstructor
public class ServerDescriptionPage  implements Serializable {

    private transient ServerEntry serverEntry;

    @Getter
    @Setter(AccessLevel.PACKAGE)
    private String longDescription;

    @Getter
    @Setter(AccessLevel.PACKAGE)
    private String host;

    @Getter
    @Setter(AccessLevel.PACKAGE)
    private int port;

    @Getter
    @Setter(AccessLevel.PACKAGE)
    private String version;

}
