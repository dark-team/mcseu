package com.darkteam.mcseu.model;

import com.darkteam.mcseu.McSeuNavigator;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.Serializable;

@AllArgsConstructor
public class ServerEntry implements Serializable {

//    @Getter
//    @Setter(AccessLevel.PACKAGE)
//    private BufferedImage serverIcon;

    private final transient McSeuNavigator navigator;

    @Getter
    @Setter(AccessLevel.PACKAGE)
    private String serverName;

    @Getter
    @Setter(AccessLevel.PACKAGE)
    private String shortDescription;

    @Getter
    @Setter(AccessLevel.PACKAGE)
    private String link;

    @Getter
    @Setter(AccessLevel.PACKAGE)
    private String id;

    @Getter
    @Setter(AccessLevel.PACKAGE)
    private int onlinePlayers;

    @Getter
    @Setter(AccessLevel.PACKAGE)
    private int maxPlayers;

    @Getter
    @Setter(AccessLevel.PACKAGE)
    private boolean online;

    @Getter
    @Setter(AccessLevel.PACKAGE)
    private int votes;

    public ServerDescriptionPage getDescription() throws IOException {
        return navigator.getDescription(this);
    }

}
