package com.darkteam.mcseu;

/**
 * The sort of your search
 */
public enum Sort {
    VOTES("rating"),
    CREATED_ASCENDING("createdASC"),
    CREATED_DESCENDING("createdDESC"),
    NAME_ASCENDING("nameASC"),
    NAME_DESCENDING("nameDESC");

    private String value;

    Sort(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }


}