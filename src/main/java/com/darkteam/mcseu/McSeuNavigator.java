package com.darkteam.mcseu;

import com.darkteam.mcseu.model.ServerDescriptionPage;
import com.darkteam.mcseu.model.ServerEntry;
import com.darkteam.mcseu.parser.SearchEntryParser;
import com.darkteam.mcseu.parser.ServerDescriptionPageParser;

import java.io.IOException;

public class McSeuNavigator {

    private SearchEntryParser searchEntryParser = new SearchEntryParser(this);

    /**
     * Returns a server array by the specified sort on specified page
     * @param page The page
     * @param sort The sort
     * @return server array
     * @throws IOException If something in the connection is wrong
     */
    public ServerEntry[] getServers(int page, Sort sort) throws IOException {
        return searchEntryParser.getServers(page, sort).toArray(new ServerEntry[0]);
    }

    /**
     * Returns description page of a minecraft server
     * @param serverEntry The server entry
     * @return The description page
     * @throws IOException If something is wrong in the connection.
     */
    public ServerDescriptionPage getDescription(ServerEntry serverEntry) throws IOException {
        return new ServerDescriptionPageParser(serverEntry).parse();
    }

}
