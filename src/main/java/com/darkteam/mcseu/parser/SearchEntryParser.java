package com.darkteam.mcseu.parser;

import com.darkteam.mcseu.McSeuNavigator;
import com.darkteam.mcseu.model.ServerEntry;
import com.darkteam.mcseu.Sort;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
@RequiredArgsConstructor
public class SearchEntryParser {

    private @NonNull McSeuNavigator navigator;

    public Collection<ServerEntry> getServers(int page, Sort sort) throws IOException {
        List<ServerEntry> serverEntries = new ArrayList<ServerEntry>();
        // user agent change, because minecraft-server.eu blocks the default jsoup agent
        Document doc = Jsoup.connect("https://minecraft-server.eu/serverlist/search/" + page + "?sort=" + sort.getValue() + "&featuresHas=&featuresHasnt=").userAgent("Mozilla/5.0 (Windows NT 6.1; WOW64; rv:5.0) Gecko/20100101 Firefox/5.0").get();
        Elements serverElements = doc.select("table > tbody > tr");
        for(Element element : serverElements){
            ServerEntry serverEntry = parseElement(element);
            if(serverEntry != null) {
                serverEntries.add(serverEntry);
            }
        }
        return serverEntries;
    }

    private ServerEntry parseElement(Element element) throws IOException {
//        Element serverIcon = element.select("img.serverIcon").first();
//        String srcString = serverIcon.attr("src");
        boolean base64 = false;
//        if(srcString.startsWith("data:")){
//            base64 = true;
//        }

        if(!element.hasAttr("onclick")) {
            return null;
        }

        Element serverNameElement = element.select("p.serverName > a").first();
        String serverName = serverNameElement.text();
        String link = serverNameElement.attr("href");

        // /server/index/[id]/[name]
        String path = new URL(serverNameElement.attr("abs:href")).getPath();
        String[] pathItems = path.split("/");
        String id = pathItems[3];

        Element serverDescElement = element.select("p.shortDesc").first();
        String description = serverDescElement.text();

        Element serverSlots = element.select("div.serverSlots").first();
        String[] slotArray = serverSlots.text().split("/", 2);

        int players = Integer.parseInt(slotArray[0]);
        int max = Integer.parseInt(slotArray[1]);

        boolean online = false;

        Element statusElement = element.select("div.serverStatus").first();
        if(statusElement.hasClass("online")) {
            online = true;
        }
        int votes = Integer.parseInt(statusElement.text());

//        byte[] imageData = null;

//        if(base64) {
//            String encodingPrefix = "base64,";
//            int contentStartIndex = srcString.indexOf(encodingPrefix) + encodingPrefix.length();
//            imageData = Base64.decodeBase64(srcString.substring(contentStartIndex));
//        } else {
//            imageData = getImageByUrl(new URL(serverIcon.attr("abs:src")));
//        }

//        BufferedImage image = null;
//        if(imageData != null) {
//            image = createImageFromBytes(imageData);
//        }

        return new ServerEntry(this.navigator, /*image, */serverName, description, link, id, players, max, online, votes);
    }

    private BufferedImage createImageFromBytes(byte[] imageData) {
        ByteArrayInputStream bais = new ByteArrayInputStream(imageData);
        try {
            return ImageIO.read(bais);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private byte[] getImageByUrl(URL url) {
        try {
            System.setProperty("http.agent", "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:5.0) Gecko/20100101 Firefox/5.0");
            InputStream in = new BufferedInputStream(url.openStream());
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            byte[] buf = new byte[1024];
            int n = 0;
            while (-1 != (n = in.read(buf))) {
                out.write(buf, 0, n);
            }
            out.close();
            in.close();
            return out.toByteArray();
        } catch(IOException ex) {
            return null;
        }
    }
}
