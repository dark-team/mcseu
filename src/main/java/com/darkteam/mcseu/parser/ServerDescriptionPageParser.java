package com.darkteam.mcseu.parser;

import com.darkteam.mcseu.model.ServerEntry;
import com.darkteam.mcseu.model.ServerDescriptionPage;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.IOException;

@RequiredArgsConstructor
public class ServerDescriptionPageParser {

    private @NonNull ServerEntry serverEntry;

    public ServerDescriptionPage parse() throws IOException {
        Document doc = Jsoup.connect("https://minecraft-server.eu/server/index/" + serverEntry.getId()).userAgent("Mozilla/5.0 (Windows NT 6.1; WOW64; rv:5.0) Gecko/20100101 Firefox/5.0").get();
        String description = doc.select("#desc").text();
        Elements serverInfo = doc.select(".serverInfo > tbody > tr");
        String ipIncludingPort = doc.select("#serverHost").text();
        String host = ipIncludingPort;
        int port = 25565;
        if(host.contains(":")) {
            String[] addressArray = ipIncludingPort.split(":", 2);
            host = addressArray[0];
            port = Integer.parseInt(addressArray[1]);
        }
        String version = serverInfo.get(1).select("td").get(1).text();
        return new ServerDescriptionPage(this.serverEntry, description, host, port, version);
    }
}
