# MCSEU Library

Since minecraft-server.eu has no API, I thought I'd write a parser with jsoup to do this. Of course this is not the most performant way, but since there is no API you have to do it differently.

## Dependency Management

### Gradle
```groovy
repositories {
	maven { url 'https://jitpack.io' }
}
dependencies {
   compile 'com.gitlab.dark-team:mcseu:1.0-SNAPSHOT'
}
	
```

### Apache Maven
```xml
<repositories>
	<repository>
	    <id>jitpack.io</id>
	    <url>https://jitpack.io</url>
	</repository>
</repositories>
<dependency>
    <groupId>com.gitlab.dark-team</groupId>
    <artifactId>mcseu</artifactId>
    <version>1.0-SNAPSHOT</version>
</dependency>
```
